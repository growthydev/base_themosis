# Configuración del proyecto para producción

####Backend

El servidor web debe apuntar a .../backend/htdocs

Dentro de un terminal entrar en la carpeta backend e instalar las dependencias de composer con
```sh
composer install
```

Obtener el hostname del servidor con
```sh
hostname
```

Modificar /backend/config/enviroment.php con el hostname del servidor.
```sh
'production' => 'hostname'
```


Las credenciales de la base de datos se almacenan en .env.production (se debe renombrar el .env.local a .env.production) que debe estar en la carpeta backend
```sh
DB_NAME = "nombre de la base de datos"
DB_USER = "usuario de mysql con permisos para la base de datos"
DB_PASSWORD = "contraseña del usuario"
DB_HOST = "direccion de la base de datos, si esta alojada en el mismo servidor suele ser 'localhost'"
WP_HOME = "http://dominio.com/"
WP_SITEURL = "http://dominio.com/cms"
```

Importar la base de datos de base.sql en la raiz del proyecto.


####Dependencias de composer 
Las dependencias más utilizadas son

-  "mailchimp/mailchimp" : generador de plantillas para envio de formularios
-  "mandrill/mandrill" : motor de envio de correos para mailchimp
-  "gabordemooij/redbean" : ORM para manipulacion de datos en php
-  "wpackagist-plugin/admin-menu-editor" : permite modificar el menu del administrador de wordpress
-  "wpackagist-plugin/post-type-archive-links" : permite utilizar archives como links en los menus del sitio
-  "yoast/wordpress-seo" : plugin que informa el estado del seo en cada una de las paginas del sitio

####Ejemplo de uso de readbean 

ver funcion readbean en

- htdocs/content/themes/themosis-theme/resources/controllers/Example.php

Y formulario en 

- htdocs/content/themes/themosis-theme/resources/views/redbeanTest.blade.php

Tabla redbean en la BD 'base' se van creando segun la necesidad