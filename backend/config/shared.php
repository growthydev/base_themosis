<?php
use RedBeanPHP\Facade as R;
/*----------------------------------------------------*/
// WordPress database
/*----------------------------------------------------*/
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', 'utf8mb4_unicode_ci');
$table_prefix = getenv('DB_PREFIX') ? getenv('DB_PREFIX') : 'wp_';

/*----------------------------------------------------*/
// Illuminate database
/*----------------------------------------------------*/
$capsule = new Illuminate\Database\Capsule\Manager();
$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => DB_HOST,
    'database'  => DB_NAME,
    'username'  => DB_USER,
    'password'  => DB_PASSWORD,
    'charset'   => DB_CHARSET,
    'collation' => DB_COLLATE,
    'prefix'    => $table_prefix
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();
$GLOBALS['themosis.capsule'] = $capsule;
R::setup( 'mysql:host='.DB_HOST.';dbname='.DB_NAME,DB_USER, DB_PASSWORD ); //for both mysql or mariaDB
/*----------------------------------------------------*/
// Authentication unique keys and salts
/*----------------------------------------------------*/
/*
 * @link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service 
 */
define('AUTH_KEY',         'SITnY<6(t-BlEkp>E3Zun)*rQwf.ZoJ|lVe`Z8GN|*5%@e9|B|c|[.3*MAw#s71+');
define('SECURE_AUTH_KEY',  'k1g4^Go,|bX&b16#-K[3&ZsWB>J~H1|.Azi`T@1.Z..VF1Aw-[lk>1)C)%-1A;IY');
define('LOGGED_IN_KEY',    '|rrHiX7[N#aG6PO+PKDH;rf;=O8w|~lNC20:|(M#.~AOcaRbH3p*H?cl(c|ew,7x');
define('NONCE_KEY',        'Xaoy^!(mut49e61A_96<P-q-=U4%|/b~N:`-W!9K/>M:ML%*EgVY6x3d+B4w~$V!');
define('AUTH_SALT',        'D+%Gy+#s%G:7TwIU%ymyVy/ykK_Y.MCJD79BH%J =7 9^|O~| |ZP{{p|oOTh#9n');
define('SECURE_AUTH_SALT', '+7D|(*7|5QCh#@I<H^dM6cQaiJ`n_Lr27}Lq7aTqvr`e+DNrJ%[bWW~C{I/jRWLh');
define('LOGGED_IN_SALT',   '^pLf0S`Cvi-x~vAS}*TRo!lQe@zF%EW!P/9G/vliDWecK{u@.H[+VA(+peu{?S19');
define('NONCE_SALT',       '|Q+-Ht ==`8C3|Z(@|RcwWap_7_%xB$-&tUi,`9@VBLgyB]T,|bNhdq=~``b:HoG');

/*----------------------------------------------------*/
// Custom settings
/*----------------------------------------------------*/
define('WP_AUTO_UPDATE_CORE', false);
define('DISALLOW_FILE_EDIT', true);
define('WP_DEFAULT_THEME', 'themosis-theme');

/* That's all, stop editing! Happy blogging. */
