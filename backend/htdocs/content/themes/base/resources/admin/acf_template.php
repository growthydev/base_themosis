<?php
/**
 * ACF has this fancy option to show ACF fields based on pages templates.
 * One problem: Themosis templates are not registered as "default" WordPress templates.
 * This file hooks into the acf filters and allows ACF to use the Themosis templates.
 */
// add themosis templates
		add_filter('acf/location/rule_values/page_template', 'add_themosis_templates_to_acf_rules');
		function add_themosis_templates_to_acf_rules($choices)
		{
			$key = 'theme'; //modified
			$configFile = 'templates.config.php'; //modified
			$configTemplates = include(themosis_path($key) . 'resources/config' . DS . $configFile ); //modified
			$templates = [];
			foreach ($configTemplates as $configTemplate) {
				$prettyTemplateName = str_replace(array('-', '_'), ' ', ucfirst(trim($configTemplate)));
				$templates[$configTemplate] = $prettyTemplateName;
			}
			return array_merge(array('none' => __('- None -')), $templates);
		}

		// get themosis templates
		add_filter('acf/location/rule_match/page_template', 'get_themosis_templates_from_acf_rules', 11, 3);
		function get_themosis_templates_from_acf_rules($match, $rule, $options)
		{

			$key = 'theme'; //modified
			$configFile = 'templates.config.php'; //modified
			$configTemplates = include(themosis_path($key) . 'resources/config' . DS . $configFile ); //modified

			// vars
			$page_template = $options['page_template'];
			// get page template
			if (!$page_template && $options['post_id']) {
				$page_template = get_post_meta($options['post_id'], '_themosisPageTemplate', true);
			}

			$page_template = ($page_template != '' && $page_template != 'none') ? $configTemplates[$page_template] : '' ;
			// compare
			if ($rule['operator'] == "==") {
				$match = ($page_template === $rule['value']);
			} elseif ($rule['operator'] == "!=") {
				$match = ($page_template !== $rule['value']);
			}
			// return
			return $match;
		}










	// LLENA LOS CAMPOS EN LA REGLA
	add_filter('acf/location/rule_values/post_template', 'acf_location_rules_values_post_template');
	function acf_location_rules_values_post_template( $choices )
	{
	   

			$key = 'theme'; //modified
			$configFile = 'templates.config.php'; //modified
			$configTemplates = include(themosis_path($key) . 'resources/config' . DS . $configFile ); //modified
			$templates = ["none"=>'- None -'];
			foreach ($configTemplates as $key => $configTemplate) {
				$field['choices'][$key] = $configTemplate;
			}
	    return $choices;
	}

	// CHECKEO PARA MOSTRAR CAMPOS EN POST ESPECÍFICO
	add_filter('acf/location/rule_match/post_template', 'acf_location_rules_match_post_template', 10, 3);
	function acf_location_rules_match_post_template( $match, $rule, $options )
	{
		global $post;

		if( ! is_object($post) ) return $match;
		
	    $current_template = $post->custom_post_template;
	    $selected_template = $rule['value'];

	  
	    if($rule['operator'] == "==")
	    {
	    	$match = ( $current_template == $selected_template );
	    }
	    if($rule['operator'] == "!=")
	    {
	    	$match = ( $current_template == $selected_template );
	    }
	    return $match;
	}

	// MUESTRA LAS OPCIONES EN EL POST
	function acf_load_custom_post_template( $field ) {
		if( ! is_admin() ) return $field;


		$key = 'theme'; //modified
		$configFile = 'templates.config.php'; //modified
		$configTemplates = include(themosis_path($key) . 'resources/config' . DS . $configFile ); //modified		
		$field['choices']["none"]='- None -';
		foreach ($configTemplates as $key => $configTemplate) {
			$field['choices'][$key] = $configTemplate;
		}

		return $field;
	 }
	add_filter('acf/load_field/name=custom_post_template', 'acf_load_custom_post_template');