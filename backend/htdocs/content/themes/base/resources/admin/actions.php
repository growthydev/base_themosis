<?php

/**
 * Define your WordPress actions for your project.
 *
 * Based on the WordPress action hooks.
 * https://developer.wordpress.org/reference/hooks/
 *
 */

define('MAPS_API_KEYS',  getenv('MAPS_API_KEYS'));

Asset::add('mainCss',themosis_assets().'/css/template.css',false,'1.0.0',false);
Asset::add('font-awesome','//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css',false,'1.0.0',false);
Asset::add('gg-jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js', false, '1.11.3', false);
Asset::add('gg-map', '//maps.google.com/maps/api/js?libraries=places&key='.MAPS_API_KEYS, false, '1.11.3', false, 'script');

Asset::add('mainJs',themosis_assets().'/js/main.js',false,'1.0.0',false, 'script');


define('FS_METHOD', 'direct');

function mmx_remove_xmlrpc_methods( $methods ) {
	unset( $methods['system.multicall'] );
	unset( $methods['system.listMethods'] );
	unset( $methods['system.getCapabilities'] );
	unset( $methods['pingback.ping'] );
	return $methods;
}
add_filter( "xmlrpc_methods", "mmx_remove_xmlrpc_methods");

add_filter( 'xmlrpc_methods', 'Remove_Unneeded_XMLRPC' );
function Remove_Unneeded_XMLRPC( $methods ) {
    unset( $methods['wp.getUsersBlogs'] );
    return $methods;
}
if(!is_user_logged_in())
	define('XMLRPC_REQUEST', false);



function disable_wp_emojicons() {

  // all actions related to emojis
//  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
//  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

  // filter to remove TinyMCE emojis
  //add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );
add_filter( 'emoji_svg_url', '__return_false' );


function quitar_barra_administracion(){
	return false;
}
add_filter( 'show_admin_bar' , 'quitar_barra_administracion');


if( function_exists('acf_add_options_sub_page') && current_user_can( 'manage_options' ))
{
	acf_add_options_sub_page( 'General' );
}

function my_acf_google_map_api( $api ){
	
	$api['key'] = MAPS_API_KEYS;
		
	return $api;
	
}

remove_action( 'shutdown', 'wp_ob_end_flush_all', 1 );