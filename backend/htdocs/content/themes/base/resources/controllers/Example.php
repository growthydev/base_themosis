<?php

namespace Theme\Controllers;

use Themosis\Route\BaseController;

use RedBeanPHP\Facade as R;

class Example extends BaseController
{
    public function index()
    {
        return view('welcome');
    }

    public function redbean($tablename="redbean") {
    	$contact = R::dispense( $tablename );
		foreach ($_POST as $key => $value):
			$contact->$key = $value;
		endforeach;

		R::store( $contact );
        wp_redirect( WP_HOME.'/', 302 );
    }

    public function redbeanTest() {

        return view('redbeanTest');
    }
}
